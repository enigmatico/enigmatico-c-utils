#include <stdio.h>
#include <signal.h>
#include <sys/socket.h>
#include <unistd.h>
#include "stack.h"
#include "net.h"

void sigsegv_trap()
{
	printf("SIGSEGV!\n");
	mm_coredump();
	exit(1);
}

int main(int args, char** argv)
{
    char* text = "Hello, Server!";
    char buf[4089] = { 0x0 };
    
    int cs = netu_clientSocket("127.0.0.1", 7080), ss = 0, rv = 0;
    
    if(!cs)
    {
        printf("Error %i\n", cs);
        return -cs;
    }
    
    printf("Connected %i\n", cs);
    ss = send(cs, text, strlen(text)+1, 0);
    printf("Sent %i bytes\n", ss);
    rv = recv(cs, buf, 4089, 0);
    printf("Received: %s\n");
    shutdown(cs, 2);
    close(cs);
	return 0;
} 
