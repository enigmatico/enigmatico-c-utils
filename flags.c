#include "flags.h"

#ifndef FLAGS_MAXFLAGS
#define FLAGS_MAXFLAGS 256
#endif

u32 flags[(FLAGS_MAXFLAGS >> 5)+1] = { 0x0L };

static inline void set_flag(unv flag)
{
	flags[flag>>5] |= (1L << flag%8);
}

static inline void inv_flag(unv flag)
{
	flags[flag>>5] ^= (1L << flag%8);
}

static inline void clr_flag(unv flag)
{
	flags[flag>>5] &= ~(1L << flag%8);
}

/* Attribute ignored in -m64 */
__attribute__((fastcall)) u8 get_flag(unv flag)
{
	return (flags[flag>>5] >> (flag%8)) & 1L;
}
