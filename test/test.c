#include <stdio.h>
#include "../eutils.h"

int main(int args, char** argv)
{
	eutils_init(ALLMODULES);
	
	char* txt = "Hello eutils!";
	char* txt2 = mm_request(strlen(txt)+1, NULL);
	char* txt3 = NULL;
	char* txt4 = NULL;
	
	memcpy(txt2,txt,strlen(txt)+1);
	
	printf("%s\n", txt2);
	
	mm_free(txt2);
	
	txt3 = mm_request(strlen(txt)+1, NULL);
	txt4 = (char*)calloc(1,strlen(txt)+1);
	
	mm_include(txt4,strlen(txt)+1);
	
	memcpy(txt3,txt,strlen(txt)+1);
	memcpy(txt4,txt,strlen(txt)+1);
	
	printf("%s | %s\n",txt3,txt4);
	
	eutils_end(ALLMODULES);
	return 0;
}
