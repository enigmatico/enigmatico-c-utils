#include <stdio.h>
#include <signal.h>
#include <sys/socket.h>
#include <unistd.h>
#include "../eutils.h"

void sigsegv_trap()
{
	printf("SIGSEGV!\n");
	mm_coredump();
	exit(1);
}

int main(int args, char** argv)
{
    int fd = netu_listenSocket(NULL, 7080, 0, NULL);
    int client = 0L, c = 0, rs = 0;
    struct sockaddr_in sClient;
    char buffer[4089] = {0x0};
    
    if(fd < 0)
    {
        printf("Error %i\n", fd);
        return fd;
    }
    
    listen(fd, 3);
    printf("Listening...\n");
    client = accept(fd, (struct sockaddr *)&sClient, (socklen_t*)&c);
    printf("Client connected %i\n", client);
    
    rs = recv(client, buffer, 4089, 0);
    printf("Received %s (%i bytes)\n", buffer, rs);
    
    send(client, buffer,rs,0);
    
    shutdown(client,2);
    close(client);
	return 0;
}
