# Stack Manager - Stack management module for C

## Introduction

Stack Manager allows the management of stacks of memory in C in a safe manner. Stack requires of the memman module in order to work. The stack can be handled as a LIFO or FIFO stack depending on the used functions. It also allows the modification of any element in the stack, if required.

## Type definitions (stack.h)

· *STACK*: Structure that contains the information of the stack.
· *st_errno*: An integer that indicates an error during st_peek and st_poke operations.
· *erdesc*: String describing an error during st_peek and st_poke operations.

## Documentation

### STACK

The definition is as follows:
```
typedef struct __stack_block__
{
	_msz szBlock; // Bytes
	_msz maxSize; // Blocks
	_msz items; // Blocks
	int status;
	void* mmBlock;
}(STACK);
```

· szBlock contains the size in bytes of a single block of memory in the stat. That is, the size of each element, in bytes.
· maxSize: The capacity of the stack, in blocks. The allocated size of the stack in bytes would be: szBlock * maxSize.
· items: The current number of items in the stack. The current size of the stack in bytes would be: items*szBlock.
· status: An integer representing the status of the stack. Only used when creating it with st_request.
· mmBlock: Allocated memory for the stack.

### st_init

Definition on stack.h:
```
int st_init(void)
```

Initializes the stack engine. It must be called upon program initialization. If memman has not been initialized, it will initialize it. So if you are planning on using both memman and stack manager, you might as well use this function instead of mm_init.

### st_end

Definition on stack.h:
```
int st_end(void)
```
This function must be called upon cleanup. It will free all previously allocated blocks of memory, and the stack. Always returns _0_.

This function does not call to mm_end, so you must call it before this function when cleaning up your memory.

### st_endall

Definition on stack.h:
```
int st_endall(void)
```
This function must be called upon cleanup. It will free all previously allocated blocks of memory, and the stack. Always returns _0_.

This function calls mm_end, so call it instead of mm_end when cleaning up both stack manager and memman before exiting your program.

### st_request

Definition on stack.h:
```
STACK st_request(_msz bSize, _msz blocks)
```

Request a new stack. This function will create a new stack, and return it's data as a STACK structure.

· bSize is the size, in bytes, of each block.
· blocks is the number of blocks.

The size of the stack heap will be bSize * blocks, in bytes.

If the function fails, it will set a status code in the status field of the returning STACK sturcture.

· -1 if Stack Manager has not been initialized (With st_init)
· -2 if bSize or blocks is 0

### st_free

Definition on stack.h:
```
int st_free(STACK* stBlock)
```
Frees a previously allocated stack, updating the STACK structure accordingly. Freed stacks will have a status 1.

On success, _0_ is returned. Otherwise, a negative value is returned.

· -1 if Stack Manager has not been initialized (With st_init)
· -2 if stBlock is NULL
· -3 if the stack was previously deallocated, has a bad status, or if it doesnt exist in the core.

### st_push

Definition on stack.h:
```
_msz st_push(STACK* stBlock, void* element)
```

Push a new element into the stack. Pushing an element will place the element in the last position of the stack.

· stBlock is a pointer to the STACK where the element will be pushed.
· element is a pointer to the element to push into the stack.

'element' must match stBlock->bSize, or it will cause undefined behavior that will most likely break your program.

If the function success, it returns the new number of items in the stack. Otherwise, _0_ is returned.

### st_pop

Definition on stack.h:
```
int st_pop(STACK* stBlock, void* rDat)
```

Pop the last element in the stack, removing it from the stack.

· stBlock is a pointer to the STACK where the last element will be popped.
· rDat is a pointer where the element will be placed. It's size must match stBlock->bSize or it will cause undefined behavior and memory corruptions.

Returns _1_ if success, or a negative value if it fails.

· -1 if Stack Manager has not been initialized (With st_init)
· -2 if the number of items in the stack is 0.

### st_shift

Definition on stack.h:
```
int st_shift(STACK* stBlock, void* element)
```

Places a new element at the bottom of the stack. It will shift the previous content one place up.

· stBlock is a pointer to the STACK where the element will be pushed.
· element is a pointer to the element to push into the stack.

'element' must match stBlock->bSize, or it will cause undefined behavior that will most likely break your program.

If the function success, it returns the new number of items in the stack. Otherwise, _0_ is returned.

### st_unshift

Definition on stack.h:
```
int st_unshift(STACK* stBlock, void* rdat)
```

Pops the first element in the stack, removing it from the stack, and moving the remaining elements one position down.

· stBlock is a pointer to the STACK where the last element will be popped.
· rDat is a pointer where the element will be placed. It's size must match stBlock->bSize or it will cause undefined behavior and memory corruptions.

Returns _1_ if success, or a negative value if it fails.

· -1 if Stack Manager has not been initialized (With st_init)
· -2 if the number of items in the stack is 0.

### st_reverse

Definition on stack.h:
```
int st_reverse(STACK* stBlock)
```

Reverses the order of the elements in the stack.

· stBlock is a pointer to the STACK whose elements will be reversed.

Returns _1_ if it success, or a negative value if it fails.

· -1 if Stack Manager has not been initialized (With st_init)
· -2 if a new memory block could not be allocated.

Be aware that this function reallocates the memory block for the stack.

### st_peek

Definition on stack.h:
```
void* st_peek(STACK* stBlock, _msz elem)
```

Returns a pointer to an element inside of the stack, so it can be read.

· stBlock is a pointer to the STACK to read from.
· elem is the number of the element in the stack.

It will return a pointer to the referenced element, or NULL if the function fails. Upon failure, st_errno will be set to a non-zero value and erdesc will be filled with more information.

### st_poke

Definition on stack.h:
```
void* st_poke(STACK* stBlock, _msz elem, void* val)
```

Changes the value of an element inside of the stack, and returns a pointer to it.

· stBlock is a pointer to the STACK to read from.
· elem is the number of the element in the stack.
· val is a pointer to the new value. It's size must match stBlock->bSize, or it will cause undefined behavior and memory corruption.

On success, it returns a pointer to the modified element, or NULL if it fails.  Upon failure, st_errno will be set to a non-zero value and erdesc will be filled with more information.

## Considerations

· Make sure that you call st_end or st_endall before exiting the program, wherever your program ends.
· Using st_free is optional, but recommended when you are not using a stack anymore, as it will free the unused memory.
· All the memory allocated by the stack core will be freed upon calling st_end or st_endall.
· Do not manually modify the members of the STACK structure returned by st_request, as they are used to keep track of your stack.
