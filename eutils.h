#ifndef __EUTILS_w14__
#define __EUTILS_w14__

#include "memman.h"
#include "stack.h"
#include "net.h"

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
#define SET_BIT(var,pos) ((var) |= (1u << (pos)))
#define CLEAR_BIT(var,pos) ((var) &= ~(1u << (pos)))

#define ALLMODULES	0
#define MEMMAN		1
#define STMAN		2

int eutils_init(int module);
int eutils_end(int module);

#endif
