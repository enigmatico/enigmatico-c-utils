#ifndef __STACK_MANAGER_H__11
#define __STACK_MANAGER_H__11

#include "memman.h"

typedef struct __stack_block__
{
	_msz szBlock; // Bytes
	_msz maxSize; // Blocks
	_msz items; // Blocks
	int status;
	void* mmBlock;
}(STACK);

int st_errno;
char erdesc[256];

int st_init();
int st_end();
int st_endall();
STACK st_request(_msz bSize, _msz blocks);
_msz st_push(STACK* stBlock, void* element);
int st_pop(STACK* stBlock, void* rDat);
int st_shift(STACK* stBlock, void* element);
int st_unshift(STACK* stBlock, void* rdat);
int st_reverse(STACK* stBlock);
void* st_peek(STACK* stBlock, _msz elem);
void* st_poke(STACK* stBlock, _msz elem, void* val);
int st_free(STACK* stBlock);
#endif