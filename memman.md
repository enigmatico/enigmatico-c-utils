# Memman - Memory manager module for C

## Introduction

This module is used to easily manage the memory usage of your program, and prevent memory leaks by tracking and organizing each memory block you request to the system. It also prevents memory corruption.

## Type definitions (memman.h)

· *NULL*: This equals an unsigned integer with value 0.
· * _msz*: This is an equivalent of _unsigned long int_
· *MMEMBLOCK*: This is a structure containing the information of the memory block. It's definition is as follows:

```
typedef struct __memblock__
{
	void* memh;
	_msz size;
}(MMEMBLOCK);
```

## Documentation

### _msz (unsigned long int)

The _msz type defines an unsigned long int used for sizes. It's value must be between 0 and the maximum value of unsigned long int for your platform.

### MMEMBLOCK

MMEMBLOCK defines a memory block structure, containing both a pointer to the memory block itself, and it's size. The structure members are:

```
void* memh
```
Void pointer to the memory block. This block is previously allocated with _calloc_, so it's content will be 0 by default.

```
_msz size
```
This is the size, in bytes, of the memory block.

*IMPORTANT*: Do NOT call _free()_ on memh. Use mm_free() on the memory block instead.

### mm_coredump

Definition on memman.h:
```
void mm_coredump(void)
```

Displays the information of the core in the console. This function should only be called when debugging your programs.

### mm_init

Definition on memman.h:
```
int mm_init(void)
```

Initializes the core of the memory manager. This function should be called upon initialization and before any other memmann function.

If the initialization success, this function returns _1_. Otherwise, it returns a non-zero value.

### mm_inc_mmblock

Definition on memman.h:
```
_msz mm_inc_mmblock(_msz addsz)
```

This function should not be called manually, as it is used internally by the core when calling the function mm_request. It's purpose is to increment the size of the core stack.

If used incorrectly, it might lead to program malfunctions.

### mm_free

Definition on memman.h:
```
int mm_free(void* _ptr)
```

Frees a previously allocated memory block.
The first parameter should be a pointer to a previously allocated memory block. Only use blocks of memory allocated with mm_request that have not been freed yet, as it will fail otherwise.

On success, it returns _0_. Otherwise, a negative value is returned, depending on the error.

· It will return -1 if _ptr is NULL.
· It will return -2 if _ptr has been dealocated, or if it's not a memory block managed by memman.

### mm_request

Definition on memman.h:
```
void* mm_request(_msz size, MMEMBLOCK** mmh)
```

Requests a memory block of size _size_. The second parameter (mmh) is optional, and it is used to fill a MMEMBLOCK pointer with the address of the MMEMBLOCK structure used by the memory block. It can be set to NULL if unused.

_size_ can not be 0 (It will return NULL if it is).

It returns a pointer to the memory block, or NULL if the function fails.

To free a memory block allocated with mm_request, use mm_free as explained above.

### mm_change_bsize

Definition on memman.h:
```
void* mm_change_bsize(void* _ptr, _msz size)
```

Changes the size of a memory block, and attempts to keep it's current data.

_ptr is a pointer to a memory block previously allocated with mm_request. size is the new size for the memory block. size is the new size of the memory block.

When expanding the size of the block (that is, size is larger than the current size),  it will keep the original data in the memory block.

However, if the size is smaller, it will trim that data to fit the new size. Whatever was beyond the new size of the memory block will be lost.

On success, it will return a pointer to the newly allocated memory block, or NULL if it fails.

The previously used block will be automatically deallocated.

### mm_realloc

Definition on memman.h:
```
void* mm_realloc(void* _ptr, _msz size)
```

Reallocates a memory block, clearing it's previous content, and initializes it to 0.

_ptr is a pointer to a memory block previously allocated with mm_request. size is the new size for the memory block. size is the new size of the memory block.

This function works like mm_change_bsize, except it doesn't respect the previous data.

On success, it will return a pointer to the newly allocated memory block, or NULL if it fails.

The previously used block will be automatically deallocated.

### mm_end

Definition on memman.h:
```
int mm_end(void)
```
This function must be called upon cleanup. It will free all previously allocated blocks of memory, and the stack. Always returns _0_.

## Considerations

· Make sure that you call mm_end before exiting the program, wherever your program ends.
· To avoid confusion and memory leaks, never mix mm_request and mm_free with calls to calloc and free. In fact, do not use these when using memman.
· Freeing memory blocks previously allocated with mm_request is optional, but recommended when they are not in use anymore, as it will keep the memory usage low. However, all allocated blocks will be automatically freed upon calling to mm_end.
