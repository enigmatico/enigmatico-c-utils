#ifndef __EU_FLAGS_H__11
#define __EU_FLAGS_H__11

#include "etypes.h"

static inline void set_flag(unv flag);
static inline void inv_flag(unv flag);
static inline void clr_flag(unv flag);
/* Attribute ignored in -m64 */
__attribute__((fastcall)) u8 get_flag(unv flag);

#endif
