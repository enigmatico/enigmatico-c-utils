#include "eutils.h"

int eutils_init(int module)
{
	int result = 0;
	if(module <= 0)
	{
		result = mm_init();
		result &= st_init();
	}
	if(CHECK_BIT(module, MEMMAN))
	{
		result = mm_init();
	}
	if(CHECK_BIT(module, STMAN))
	{
		result = st_init();
	}
	return result;
}

int eutils_end(int module)
{
	int result = 0;
	if(module <= 0)
	{
		result = mm_end();
		result &= st_end();
	}
	if(CHECK_BIT(module, MEMMAN))
	{
		result = mm_end();
	}
	if(CHECK_BIT(module, STMAN))
	{
		result = st_end();
	}
	return result;
}
