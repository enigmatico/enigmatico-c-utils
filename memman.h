#ifndef __MEM_MANAGER_H__11
#define __MEM_MANAGER_H__11

#ifndef NULL
#define NULL 0L
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define _msz unsigned long int

typedef struct __memblock__
{
	void* memh;
	_msz size;
}(MMEMBLOCK);

void mm_coredump(void);
int mm_init(void);
_msz mm_inc_mmblock(_msz addsz);
int mm_free(void* _ptr);
void* mm_request(_msz size, MMEMBLOCK** mmh);
void* mm_change_bsize(void* _ptr, _msz size);
void* mm_realloc(void* _ptr, _msz size);
void* mm_include(void* _ptr, _msz size);
int mm_end(void);

#endif 
