# Enigmatico's Utility Library for C
## eutils

For all these tedious tasks in C, I present you a very lightweight static library that contains modules for memory management and stack management.

This code will surely save you some hours of tedious hard work and some problems.

## How to use it

You can either build the static library

```
$ make static
```

And then copy it into your project along with the header files, or directly use the .c files into your project as if it was another part of it. The choice is yours.

No libraries or prerequisites are required to use these tools. In fact, it should work for all systems, even ancient ones. Even Windows ones. Even consoles! Weapons! Robots! Anything! As long as there is a C compiler with the standard C library for said platform.

The weapon part is a joke. Please don't make Skynet robots by using this software.

## Documentation

In-depth documentation for each module is included with the source:

Memman module
https://github.com/Senjihakku/eutils/blob/master/memman.md

Stack module
https://github.com/Senjihakku/eutils/blob/master/stack.md
